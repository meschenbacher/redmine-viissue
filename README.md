# redmine-viissue

Edit redmine issue descriptions and wiki pages with your favourite editor (chosen via
environment variables `VIISSUE_EDITOR` over `VISUAL` over `EDITOR` over vim).

# Requirements

- python packages (e.g. via pip): `python-redmine`
- redmine URL and write token as environment variable `REDMINE_URL` and `REDMINE_TOKEN`

# install

```
virtualenv -p python3 venv
pip install -r requirements.txt
```

Shell alias for setting environment variables and calling the script

```
alias export_redmine="export REDMINE_URL=https://your-redmine-instance.de REDMINE_TOKEN=$(pass show redminetoken)"
alias viissue="~/git/redmine-viissue/venv/bin/python ~/git/redmine-viissue/viissue.py"
```

# Usage

Once per shell

    export_redmine

Otherwise

    viissue -c 1234
    viissue -c https://your-redmine-instance.de/issues/1234

Or

    viissue -c https://your-redmine-instance.de/project/name/wiki/page
