#!/usr/bin/env python3

# Requirements:
# packages
# python-redmine (python)
# env variables
# REDMINE_URL REDMINE_TOKEN REQUEST_CA_BUNDLE

import sys, os
import argparse
from redminelib import Redmine
from redminelib.exceptions import ResourceNotFoundError
from redminelib.resources import Issue, WikiPage
import tempfile
from subprocess import call
import difflib
import re

parser = argparse.ArgumentParser(description="""Edit redmine entities (issue descriptions and wiki pages) with your favourite editor""")
parser.add_argument('-c', '--confirm', action='store_true', help='Show a diff and confirm changes')
parser.add_argument('identifier', metavar='TICKET_ID', type=str, help='ticket id, ticket url, or wiki page url')

EDITOR = os.environ.get('VIISSUE_EDITOR', os.environ.get('VISUAL', os.environ.get('EDITOR', 'vim')))

redmine = Redmine(
    os.environ['REDMINE_URL'],
    key=os.environ['REDMINE_TOKEN'],
)

def edit(context):
    initial_message = context

    with tempfile.NamedTemporaryFile(suffix=".tmp") as tf:
        tf.write(initial_message.encode('utf-8'))
        tf.flush()
        call([EDITOR, tf.name])
        tf.seek(0)
        edited_message = tf.read()
        return edited_message.decode('utf-8')

def exit_notfound():
    print(args.identifier, "is neither a ticket ID nor a valid redmine wiki url")
    sys.exit(1)

def obtain_entity(identifier):
    """
    use identifier from command line and try to return the entity as well as the initial
    text which can be edited.
    """
    entity = None
    initial_context = None
    try:
        entity = redmine.issue.get(args.identifier)
        initial_context = entity.description
    except ResourceNotFoundError:
        pass

    try:
        # issue ids are unique per redmine instance
        m = re.search(r'/issues/(\d+)', args.identifier)
        if m:
            entity = redmine.issue.get(m.group(1))
            initial_context = entity.description
    except ResourceNotFoundError:
        pass

    try:
        # wiki titles are unique per project and do not contain slashes nor hierarchy in
        # the url
        m = re.search(r'/projects/([^/]+)/wiki/([^/]+)', args.identifier)
        if m:
            entity = redmine.wiki_page.get(resource_id=m.group(2), project_id=m.group(1))
            initial_context = entity.text
    except ResourceNotFoundError:
        pass
    return entity, initial_context

def set_context(entity, context) -> None:
    """
    we're dealing with two types of entities and need to abstract writes to the actual
    content
    """
    if isinstance(entity, Issue):
        entity.description = context
    elif isinstance(entity, WikiPage):
        entity.text = context
    else:
        raise Exception("type not implemented")

def main(args):

    entity, initial_context = obtain_entity(args.identifier)
    if not entity:
        exit_notfound()

    context = initial_context
    while True:
        context = edit(context)

        if args.confirm:
            sys.stdout.writelines(
                difflib.unified_diff(
                    [ s + '\n' for s in initial_context.split('\n') ],
                    [ s + '\n' for s in context.split('\n') ],
                    fromfile='before', tofile='after',
                )
            )
            ans = input("Confirm save [Y/n/q(uit without save)]? ")
            if ans in ["q", "Q"]:
                sys.exit(0)
            if ans not in ["", "Y", "y"]:
                continue
        break
    set_context(entity, context)
    entity.save()

if __name__ == '__main__':
    args = parser.parse_args()
    main(args)
